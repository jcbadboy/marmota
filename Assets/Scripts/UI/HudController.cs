﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudController : BaseSingleton<HudController> {

    [Header("UI References")]
    [SerializeField] private Button ButtonPause;

    [Header("Resources")]
    public Sprite PausedSprite;
    public Sprite UnPausedSprite;

    public delegate void OnPauseEvent();
    public event OnPauseEvent OnPause;


    #region Public Methods
    public void ShowPausedFeedBack()
    {
        ((Image)ButtonPause.targetGraphic).sprite = PausedSprite;
    }

    public void HidePausedFeedBack()
    {
        ((Image)ButtonPause.targetGraphic).sprite = UnPausedSprite;
    }
    #endregion

    #region UI Interactions
    public void OnButtonPressed_Pause()
    {
        if (OnPause != null)
            OnPause();
    }
    #endregion
}
